import React from "react";
import "./App.less";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Layout, { Header, Content, Footer } from "antd/lib/layout/layout";
import Sider from "antd/lib/layout/Sider";
import Sidebar from "./components/sidebar/Sidebar";
import ChatContainer from "./components/chat-container/ChatContainer";
import dayjs from "dayjs";
import de from "dayjs/locale/de";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faBriefcase,
  faCheckCircle,
  faEuroSign,
  faFolder,
  faMapMarkerAlt,
  faBars,
} from "@fortawesome/free-solid-svg-icons";
import useBreakpoint from "antd/lib/grid/hooks/useBreakpoint";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

dayjs.locale(de);

library.add(
  faMapMarkerAlt,
  faBriefcase,
  faFolder,
  faEuroSign,
  faCheckCircle,
  faBars
);

const SiteContent = () => {
  return (
    <>
      <Route
        path="/"
        exact
        component={() => (
          <Content>
            <ChatContainer />
          </Content>
        )}
      />
    </>
  );
};

const App = () => {
  const { xl } = useBreakpoint();
  return (
    <Router>
      {xl && (
        <Layout>
          <Sider breakpoint="lg" collapsedWidth="0">
            <Sidebar />
          </Sider>
          <Layout style={{ height: "100vh" }}>
            <Header
              style={{
                backgroundColor: "white",
                boxShadow: "0px 0px 20px rgba(0,0,0,0.1)",
                marginBottom: "1rem",
              }}
            >
              <div style={{ fontWeight: "bold" }}>Keb.App</div>
            </Header>

            <SiteContent />
          </Layout>
        </Layout>
      )}
      {!xl && (
        <Layout style={{ height: "100vh" }}>
          <Header
            style={{
              boxShadow: "0px 0px 20px rgba(0,0,0,0.1)",
              marginBottom: "1rem",
            }}
          >
            <div
              style={{
                color: "white",
                fontWeight: "bold",
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              Keb.App
              <FontAwesomeIcon
                icon={"bars"}
                color="white"
                size="2x"
                onClick={() => console.log("burger click")}
              />
            </div>
          </Header>
          <SiteContent />
        </Layout>
      )}
    </Router>
  );
};

export default App;
