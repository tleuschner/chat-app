import * as images from "./images";

export interface ConversationListData {
  id: string;
  icon: string; // base64,
  company: string;
  lastMessage: {
    message: string;
    timestamp: number;
  };
  jobDescription: string;
  chatId: string;
}

export const applicant = "Tim";
export const kebapp = "KebApp";
export const matched = "matched.io";
export const facebook = "Facebook";

export const conversationListData: ConversationListData[] = [
  {
    id: "asdf",
    company: kebapp,
    icon: images.kebappImage,
    jobDescription:
      "Full Stack Engineer TypeScript, React Native, Google Cloud",
    lastMessage: {
      message: "So machen wir das!",
      timestamp: Date.now(),
    },
    chatId: kebapp,
  },
  {
    id: "asdfg",
    company: matched,
    icon: images.matchedImage,
    jobDescription:
      "Full Stack Engineer TypeScript, React Native, Google Cloud",
    lastMessage: {
      message: "Alles klar!",
      timestamp: Date.now() - 43200000,
    },
    chatId: matched,
  },
  {
    id: "asdfgh",
    company: facebook,
    icon: images.facebookImage,
    jobDescription:
      "Full Stack Engineer TypeScript, React Native, Google Cloud",
    lastMessage: {
      message: "",
      timestamp: Date.now(),
    },
    chatId: facebook,
  },
];

export interface ChatMessage {
  message: string;
  id: string;
  from: string;
  timestamp: number;
  name: string;
}

export interface ChatMessageWithChatId extends ChatMessage {
  chatId: string;
}

export const kebappChatMessages: ChatMessage[] = [
  {
    message: "Hey, coole App, die ihr habt. Braucht ihr noch einen Entwickler?",
    from: applicant,
    id: "1",
    name: applicant,
    timestamp: Date.now() - 43200000 * 5,
  },
  {
    message: "Moin Tim, ja klar, was hast du denn drauf?",
    from: kebapp,

    id: "2",
    name: kebapp,
    timestamp: Date.now() - 43200000 * 4,
  },
  {
    message:
      "Ich habe bis jetzt Erfahrung in ReactNative sammeln können und bin sonst auch ganz gut mit Angular untwergs ✌️",
    from: applicant,

    id: "3",
    name: applicant,

    timestamp: Date.now() - 43200000 * 3,
  },
  {
    message:
      "Cool, lass uns gemeinsam einen Döner essen gehen und wir sprechen weiter 😋",
    from: kebapp,

    id: "4",
    name: kebapp,

    timestamp: Date.now() - 43200000 * 2,
  },
  {
    message: "So machen wir das!",
    from: applicant,

    id: "5",
    name: applicant,

    timestamp: Date.now() - 43200000,
  },
];

export const matchedChatMessages: ChatMessage[] = [
  {
    message:
      "Hey, coole Konzept, was ihr habt. Braucht ihr noch einen Entwickler?",
    from: applicant,
    id: "6",
    name: applicant,
    timestamp: Date.now() - 43200000 * 5,
  },
  {
    message: "Moin Tim, ja klar, was machst du denn?",
    from: matched,

    id: "7",
    name: matched,
    timestamp: Date.now() - 43200000 * 4,
  },
  {
    message:
      "Ich habe bis jetzt Erfahrung in ReactNative sammeln können und sonst ein wenig Webentwicklung ✌️",
    from: applicant,

    id: "8",
    name: applicant,

    timestamp: Date.now() - 43200000 * 3,
  },
  {
    message: "Cool, lass uns besprechen und dann schauen wir mal.",
    from: matched,

    id: "9",
    name: matched,

    timestamp: Date.now() - 43200000 * 2,
  },
  {
    message: "Alles klar!",
    from: applicant,

    id: "10",
    name: applicant,

    timestamp: Date.now() - 43200000,
  },
];
