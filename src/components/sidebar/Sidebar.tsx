import React from "react";

import { Menu } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { useDispatch } from "react-redux";
import { setUserId } from "../../app/actions";
import { applicant, facebook, kebapp, matched } from "../../mockData";

const { Item } = Menu;

const Sidebar = () => {
  const dispatch = useDispatch();

  return (
    <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
      <Item
        key="1"
        icon={<UserOutlined />}
        onClick={() => dispatch(setUserId(applicant))}
      >
        Bewerber
      </Item>
      <Item
        key="2"
        icon={<UserOutlined />}
        onClick={() => dispatch(setUserId(kebapp))}
      >
        KebApp
      </Item>
      <Item
        key="3"
        icon={<UserOutlined />}
        onClick={() => dispatch(setUserId(matched))}
      >
        matched.io
      </Item>
      <Item
        key="4"
        icon={<UserOutlined />}
        onClick={() => dispatch(setUserId(facebook))}
      >
        Facebook
      </Item>
    </Menu>
  );
};

export default Sidebar;
