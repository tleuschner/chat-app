import React from "react";

interface LogoProps {
  icon: string;
}

const Logo = ({ icon }: LogoProps) => {
  return (
    <span
      className="logo"
      style={{ padding: "0 1rem", display: "flex", alignItems: "center" }}
    >
      <img
        src={icon}
        alt="company-logo"
        style={{
          height: "3rem",
          width: "3rem",
          borderRadius: "1.5rem",
        }}
      />
    </span>
  );
};

export default Logo;
