import { ChartConfiguration } from "chart.js";

export const chartOptions: ChartConfiguration = {
  type: "radar",
  data: {
    datasets: [
      {
        data: [50, 20, 30, 40, 30, 20, 20, 30],
      },
    ],
    labels: [
      "React",
      "CSS",
      "Redux",
      "Less",
      "Sass",
      "Python",
      "Django",
      "MySQL",
    ],
  },
  options: {
    legend: {
      display: false,
    },
    responsive: true,
    scale: {
      ticks: {
        beginAtZero: true,
        fontSize: 0,
        stepSize: 10,
      },
    },
  },
};
