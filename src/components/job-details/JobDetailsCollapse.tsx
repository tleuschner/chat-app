import React, { useEffect, useState } from "react";
import { UpOutlined } from "@ant-design/icons";
import { Collapse } from "antd";
import "./JobDetails.css";
import JobDetails from "./JobDetails";

const { Panel } = Collapse;
interface JobDetailsCollapseProps {
  selected: boolean;
}

const JobDetailsCollapse = ({ selected }: JobDetailsCollapseProps) => {
  const [activePanel, setActivePanel] = useState(selected ? "1" : "");

  useEffect(() => {
    setActivePanel(selected ? "1" : "");
  }, [selected]);

  return (
    <Collapse
      style={{ backgroundColor: "white" }}
      ghost
      expandIcon={() => null}
      accordion
      activeKey={activePanel}
      onChange={(key) => setActivePanel(key as string)}
    >
      <Panel
        header={
          <div style={{ display: "flex", flex: 1, justifyContent: "center" }}>
            <UpOutlined
              className={`${activePanel === "1" ? "open" : "closed"}`}
            />
          </div>
        }
        key="1"
      >
        <JobDetails />
      </Panel>
    </Collapse>
  );
};

export default JobDetailsCollapse;
