import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Row, Col, Button } from "antd";
import Chart from "chart.js";
import React, { memo, useCallback } from "react";
import { chartOptions } from "./chartOptions";

interface JobeDetailsData {
  overview: {
    location: string;
    employmentType: string;
    branch: string;
    salary: {
      from: string;
      to: string;
    };
    remote: {
      allowed: boolean;
      types: string[];
    };
    hoursPerWeek: string;
    managementFunctions: boolean;
    customerContact: string;
    requiredLanguages: string[];
    workingOn: string;
    benefits: string[];
  };
}

const mockData: JobeDetailsData = {
  overview: {
    location: "Hamburg",
    employmentType: "Festanstellung",
    branch: "Internet und Informationstechnologie",
    salary: {
      from: "40.000",
      to: "50.000",
    },
    remote: {
      allowed: true,
      types: ["Remote-Arbeit", "Homeoffice"],
    },
    hoursPerWeek: "40",
    managementFunctions: false,
    customerContact: "Keinen Kontakt",
    requiredLanguages: ["Deutsch"],
    workingOn: "Produkt",
    benefits: [
      "Flexible Arbeitszeiten",
      "Hunde erlaubt",
      "Firmenwagen",
      "Events",
    ],
  },
};

const JobDetails = () => {
  const drawChart = useCallback((node: HTMLCanvasElement | null) => {
    const context = node?.getContext("2d");
    context && new Chart(context, chartOptions);
  }, []);

  return (
    <div style={{ maxHeight: "30vh", overflowY: "auto" }}>
      <Row>
        <Col>
          <div className="small-headline">Übersicht</div>
          <Row>
            <Col
              lg={{ span: 8 }}
              sm={{ span: 12 }}
              xs={{ span: 24 }}
              className="description"
            >
              <FontAwesomeIcon
                icon={"map-marker-alt"}
                className="icon-margin"
              />
              {mockData.overview.location}
            </Col>
            <Col
              lg={{ span: 8 }}
              sm={{ span: 12 }}
              xs={{ span: 24 }}
              className="description"
            >
              <FontAwesomeIcon icon={"briefcase"} className="icon-margin" />
              {mockData.overview.employmentType}
            </Col>
            <Col
              lg={{ span: 8 }}
              sm={{ span: 12 }}
              xs={{ span: 24 }}
              className="description"
            >
              <FontAwesomeIcon icon={"folder"} className="icon-margin" />
              {mockData.overview.branch}
            </Col>
            <Col
              lg={{ span: 8 }}
              sm={{ span: 12 }}
              xs={{ span: 24 }}
              className="description"
            >
              <FontAwesomeIcon icon={"euro-sign"} className="icon-margin" />
              {mockData.overview.salary.from} - {mockData.overview.salary.to}
            </Col>
            <Col
              lg={{ span: 8 }}
              sm={{ span: 12 }}
              xs={{ span: 24 }}
              className="description"
            >
              <FontAwesomeIcon icon={"check-circle"} className="icon-margin" />
              {mockData.overview.remote.types.join(", ")}
            </Col>
          </Row>
        </Col>
      </Row>
      <Row style={{ marginTop: "1.5rem" }}>
        <Col style={{ flex: 1 }}>
          <div className="small-headline">Tech Stack</div>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              padding: "1rem 0",
            }}
          >
            <canvas ref={(ref) => drawChart(ref)}></canvas>
          </div>
        </Col>
      </Row>
      <Row style={{ marginTop: "1.5rem" }}>
        <Col>
          <div className="small-headline">Details</div>
          <div
            style={{
              padding: "1rem 0",
              maxHeight: "15vh",
              overflow: "hidden",
            }}
          >
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Excepturi
            nobis soluta quam enim! Nostrum, unde aspernatur non facere,
            quisquam, blanditiis labore asperiores consectetur laudantium
            quibusdam impedit sed! Deserunt, nihil. Voluptates!
            <br />
            <br />
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae, autem
            velit? Cum inventore esse iure dicta praesentium officia omnis? Odio
            placeat nam laboriosam, maiores dignissimos earum quo iure? Fuga,
            adipisci.
          </div>
          <div className="fadeout"></div>
          <Button
            type="primary"
            style={{
              backgroundColor: "rgb(92, 125, 248)",
              borderColor: "rgb(92, 125, 248)",
              margin: "-3rem auto",
              display: "block",
            }}
          >
            Alle Jobdetails
          </Button>
        </Col>
      </Row>
    </div>
  );
};

export default memo(JobDetails);
