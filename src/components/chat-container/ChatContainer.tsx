import React, { useEffect } from "react";
import Chat from "../chat/Chat";
import ConversationList from "../conversations-list/ConversationList";
import { Row } from "antd";
import { useTypedSelector } from "../../hooks/useTypedSelector";
import useSocket from "../../hooks/useSocket";
import { useDispatch } from "react-redux";
import { setSocket } from "../../app/actions";
import useBreakpoint from "antd/lib/grid/hooks/useBreakpoint";

const ChatContainer = () => {
  const dispatch = useDispatch();
  const {
    currentChatId,
    currentChatPartner,
    conversationList,
  } = useTypedSelector((state) => state.chat);
  const { userId, showChat } = useTypedSelector((state) => state.app);
  const { lg } = useBreakpoint();
  const socket = useSocket(
    userId,
    conversationList.map((p) => p.chatId)
  );

  useEffect(() => {
    dispatch(setSocket(socket));
  }, [socket, dispatch]);

  return (
    <div className="site-padding">
      <Row style={{ padding: "0 1rem" }}>
        {/*always show on large screen, only show on smaller screen if chat isnt open */}
        {(lg || (!lg && !showChat)) && (
          <ConversationList
            chatId={currentChatId}
            conversationListData={conversationList}
            isLargeScreen={Boolean(lg)}
          />
        )}

        {lg && currentChatPartner && currentChatId && (
          <Chat {...{ currentChatPartner, currentChatId }} />
        )}
        {!lg && currentChatPartner && currentChatId && showChat && (
          <Chat {...{ currentChatPartner, currentChatId }} mobileView />
        )}
      </Row>
    </div>
  );
};

ChatContainer.whyDidYouRender = true;

export default ChatContainer;
