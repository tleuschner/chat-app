import { LeftOutlined } from "@ant-design/icons";
import { Button, Col, Row, Space } from "antd";
import useBreakpoint from "antd/lib/grid/hooks/useBreakpoint";
import React from "react";
import { useDispatch } from "react-redux";
import { toggleChat } from "../../../app/actions";
import Logo from "../../Logo";

interface ChatHeaderProps {
  icon: string;
  company: string;
  height: string;
  mobileView?: boolean;
}

const ChatHeader = ({ company, icon, height, mobileView }: ChatHeaderProps) => {
  const dispatch = useDispatch();
  const { xs } = useBreakpoint();
  return (
    <>
      {mobileView && (
        <Row>
          <Col
            style={{ cursor: "pointer" }}
            onClick={() => dispatch(toggleChat())}
          >
            <LeftOutlined /> Zurück
          </Col>
        </Row>
      )}
      <Row
        wrap={false}
        align="middle"
        style={{ maxHeight: height, minHeight: height }}
      >
        <Col>
          <Logo icon={icon} />
        </Col>
        <Col style={{ flex: 1 }}>
          <Row>
            <Col
              span={24}
              style={{ fontWeight: "bold", fontSize: 20, marginBottom: 5 }}
            >
              {company}
            </Col>
            <Col span={24} style={{ flex: 1 }}>
              {xs ? (
                <Button
                  type="ghost"
                  style={{
                    color: "#5c7df8",
                    borderColor: "#5c7df8",
                    padding: "0 4px",
                  }}
                >
                  Mehr erfahren
                </Button>
              ) : (
                <div
                  style={{
                    display: "flex",
                    flex: 1,
                    justifyContent: "space-between",
                  }}
                >
                  <Space size="small">
                    <Button
                      type="ghost"
                      style={{
                        color: "#5c7df8",
                        borderColor: "#5c7df8",
                        padding: "0 4px",
                      }}
                    >
                      Unternehmensprofil
                    </Button>
                    <Button
                      type="ghost"
                      style={{
                        color: "#5c7df8",
                        borderColor: "#5c7df8",
                        padding: "0 4px",
                      }}
                    >
                      Jobdetails
                    </Button>
                  </Space>
                  <Button type="primary" style={{ backgroundColor: "#5c7df8" }}>
                    Eingestellt 💸
                  </Button>
                </div>
              )}
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
};

export default ChatHeader;
