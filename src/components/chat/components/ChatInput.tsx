import React, { useEffect, useState } from "react";
import { SendOutlined } from "@ant-design/icons";
import { Row, Col, Button } from "antd";
import TextArea from "antd/lib/input/TextArea";
import { ChatMessageWithChatId } from "../../../mockData";
import { useDispatch } from "react-redux";
import { receiveMessage, sendMessage } from "../../../app/actions";
import { useTypedSelector } from "../../../hooks/useTypedSelector";

interface ChatInputProps {
  chatId: string;
}

const ChatInput = ({ chatId }: ChatInputProps) => {
  const dispatch = useDispatch();
  const { userId } = useTypedSelector((state) => state.app);
  const [message, setMessage] = useState<string>("");
  const socket = useTypedSelector((state) => state.app.socket);

  useEffect(() => {
    socket?.on("RECEIVE_MESSAGE", (message: ChatMessageWithChatId) => {
      dispatch(receiveMessage(message));
    });
  }, [socket, dispatch]);

  useEffect(() => {
    setMessage(localStorage.getItem(chatId) || "");
  }, [chatId]);

  return (
    <Row>
      <Col
        style={{
          flex: 1,
          display: "flex",
          alignItems: "center",
          backgroundColor: "#fff",
        }}
      >
        <TextArea
          style={{
            margin: "1rem",
            padding: "1rem",
            borderRadius: ".2rem",
            resize: "vertical",
          }}
          className="text-area"
          value={message}
          onChange={({ target: { value } }) => {
            localStorage.setItem(chatId, value);
            setMessage(value);
          }}
          autoSize
        />

        <Button
          type="primary"
          shape="circle"
          icon={<SendOutlined />}
          size="large"
          style={{ margin: "0 24px 0 0" }}
          onClick={() => {
            const chatMessage: ChatMessageWithChatId = {
              from: userId,
              id: (Math.random() * 100000000).toFixed(0),
              message: message,
              name: userId,
              timestamp: Date.now(),
              chatId,
            };

            socket?.emit("SEND_MESSAGE", chatMessage);
            dispatch(sendMessage(chatMessage));
            setMessage("");
            localStorage.setItem(chatId, "");
          }}
        />
      </Col>
    </Row>
  );
};

export default ChatInput;
