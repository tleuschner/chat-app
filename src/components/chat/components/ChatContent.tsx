import { Row, Col } from "antd";
import React, { useEffect, useRef, useState } from "react";
import dayjs from "dayjs";
import ChatBubble from "./ChatBubble";
import DateDivider from "./DateDivider";
import { useTypedSelector } from "../../../hooks/useTypedSelector";
import JobDetails from "../../job-details/JobDetails";

interface ChatContentProps {
  chatId: string;
  mobileView: boolean;
}

const ChatContent = ({ chatId, mobileView }: ChatContentProps) => {
  const chatMessages = useTypedSelector(
    (state) => state.chat.chatMessages[chatId]
  );
  const { userId } = useTypedSelector((state) => state.app);
  const [headerHasJobDetails, setHeaderHasJobDetails] = useState<boolean>(true);

  const messagesEndRef = useRef<HTMLDivElement | null>();

  useEffect(() => {
    if (messagesEndRef.current) {
      messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
    }
  }, [chatMessages]);

  useEffect(() => {
    const userWroteMessage = Boolean(
      chatMessages.find((m) => m.from === userId)
    );
    setHeaderHasJobDetails(!userWroteMessage);
  }, [chatMessages, userId]);

  return (
    <Row>
      <Col span={24}>
        {headerHasJobDetails && mobileView && <JobDetails />}
        {headerHasJobDetails && (
          <div
            style={{
              margin: ".5rem auto 0 auto",
              display: "flex",
              justifyContent: "center",
            }}
          >
            <p style={{ textAlign: "center", margin: 0 }}>
              Sieht ziemlich leer aus, mach den ersten Schritt ✌️
            </p>
          </div>
        )}
        {chatMessages.map((message, index) => {
          let needsDateDivider = false;
          const prevMesseage = chatMessages[index - 1];
          if (prevMesseage) {
            const { timestamp: prevMesseageTimestamp } = prevMesseage;
            const { timestamp: nextMesseageTimestamp } = message;
            const prevMessageHour = Number(
              dayjs(prevMesseageTimestamp).format("HH")
            );
            const nextMessageHour = Number(
              dayjs(nextMesseageTimestamp).format("HH")
            );
            needsDateDivider =
              (prevMessageHour <= 24 && nextMessageHour < prevMessageHour) ||
              dayjs(prevMesseageTimestamp).diff(nextMesseageTimestamp, "d") > 0;
          }
          return (
            <div key={message.id}>
              {(needsDateDivider || !prevMesseage) && (
                <DateDivider date={message.timestamp} key={message.timestamp} />
              )}
              <ChatBubble {...message} myMessage={message.from === userId} />
            </div>
          );
        })}
        <div ref={(ref) => (messagesEndRef.current = ref)}></div>
      </Col>
    </Row>
  );
};

export default ChatContent;
