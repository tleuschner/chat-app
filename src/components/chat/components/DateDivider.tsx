import { Divider } from "antd";
import dayjs from "dayjs";
import React from "react";

interface DateDividerProps {
  date: number;
}

const DateDivider = ({ date }: DateDividerProps) => {
  return <Divider>{dayjs(date).format("dddd, D MMMM")}</Divider>;
};

export default DateDivider;
