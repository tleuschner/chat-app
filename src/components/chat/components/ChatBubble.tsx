import { Col, Row } from "antd";
import dayjs from "dayjs";
import React from "react";
import { ChatMessage } from "../../../mockData";
import "./ChatBubble.css";

interface ChatBubbleProps extends ChatMessage {
  myMessage: boolean;
}

const ChatBubble = ({
  myMessage,
  message,
  name,
  timestamp,
}: ChatBubbleProps) => {
  return (
    <Row justify={myMessage ? "end" : "start"} style={{ margin: "1rem auto" }}>
      <Col>
        <div className={`box ${myMessage ? "right" : "left"}`}>
          {!myMessage && <div style={{ paddingBottom: ".5rem" }}>{name}</div>}
          {message}
        </div>
        <div style={{ float: myMessage ? "right" : "left" }}>
          {dayjs(timestamp).format("HH:mm")}
        </div>
      </Col>
    </Row>
  );
};

export default ChatBubble;
