import { Card, Col } from "antd";
import React from "react";

import { ConversationListData } from "../../mockData";
import ChatContent from "./components/ChatContent";
import ChatHeader from "./components/ChatHeader";
import ChatInput from "./components/ChatInput";

interface ChatProps {
  currentChatPartner: ConversationListData;
  currentChatId: string;
  mobileView?: boolean;
}

const Chat = ({
  currentChatPartner: { company, icon },
  currentChatId,
  mobileView,
}: ChatProps) => {
  return (
    <Col lg={12} style={{ flex: 1 }}>
      <Card
        title={
          <ChatHeader
            {...{ company, icon, height: "10vh" }}
            mobileView={mobileView}
          />
        }
      >
        <div
          style={{
            maxHeight: "60vh",
            overflowY: "scroll",
            padding: "0 1.5rem 0 1.5rem",
            overflowX: "hidden",
          }}
        >
          <ChatContent
            chatId={currentChatId}
            mobileView={Boolean(mobileView)}
          />
        </div>
      </Card>
      <ChatInput chatId={currentChatId} />
    </Col>
  );
};

export default Chat;
