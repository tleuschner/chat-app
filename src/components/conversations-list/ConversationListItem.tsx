import React from "react";
import { ConversationListData } from "../../mockData";
import dayjs from "dayjs";
import Logo from "../Logo";
import { Col, Row } from "antd";
import JobDetails from "../job-details/JobDetailsCollapse";
import { useDispatch } from "react-redux";
import { toggleChat } from "../../app/actions";
import "./ConversationListItem.css";

interface ConversationListItemProps extends ConversationListData {
  selected: boolean;
  onClick: () => void;
  isLargeScreen: boolean;
}

const ConversationListItem = ({
  company,
  icon,
  jobDescription,
  lastMessage,
  selected,
  onClick,
  isLargeScreen,
}: ConversationListItemProps) => {
  const dispatch = useDispatch();
  return (
    <div
      onClick={() => {
        !isLargeScreen && dispatch(toggleChat());
        onClick();
      }}
      style={{
        minHeight: "4rem",
      }}
    >
      <Row
        align="middle"
        wrap={false}
        style={{
          padding: "1rem 1rem 1rem 0",
          backgroundColor: selected ? "#e0e0e0" : "white",
        }}
      >
        <Col>
          <Logo icon={icon} />
        </Col>
        <Col style={{ minWidth: 0, flex: 1 }}>
          <Row
            justify="space-between"
            wrap={false}
            style={{ marginBottom: ".1rem" }}
          >
            <Col style={{ fontWeight: "bold" }}>{company}</Col>
            <Col style={{ fontSize: "12px", color: "rgba(0,0,0,0.5)" }}>
              {dayjs(lastMessage.timestamp).format("DD.MM.YYYY HH:mm")}
            </Col>
          </Row>
          <Row>
            <Col
              style={{ color: "rgb(67, 208, 150)" }}
              className="truncate-text"
            >
              für {jobDescription}
            </Col>
          </Row>
          <Row>
            <Col>
              <div
                style={{
                  flex: 1,
                }}
                className="truncate-text"
              >
                {lastMessage.message}
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      {isLargeScreen && <JobDetails selected={selected} />}
    </div>
  );
};

export default ConversationListItem;
