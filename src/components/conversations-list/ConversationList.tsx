import React from "react";
import { Col, Divider } from "antd";
import { useDispatch } from "react-redux";
import { switchChatRooms } from "../../app/actions";
import { useTypedSelector } from "../../hooks/useTypedSelector";
import { ConversationListData } from "../../mockData";
import ConversationListItem from "./ConversationListItem";

interface ConversationListProps {
  chatId: string | null;
  conversationListData: ConversationListData[];
  isLargeScreen: boolean;
}

const ConversationList = ({
  chatId,
  conversationListData,
  isLargeScreen,
}: ConversationListProps) => {
  const dispatch = useDispatch();
  const { currentChatId } = useTypedSelector((state) => state.chat);
  const selectChat = (chatId: string) => {
    if (chatId !== currentChatId) {
      dispatch(switchChatRooms(chatId));
    }
  };

  return (
    <Col lg={12} md={24} style={{ flex: 1 }}>
      {conversationListData.map((item) => (
        <div key={item.id}>
          <ConversationListItem
            {...item}
            selected={chatId === item.chatId}
            onClick={() => selectChat(item.chatId)}
            isLargeScreen={isLargeScreen}
          />
          <Divider style={{ margin: 0 }} />
        </div>
      ))}
    </Col>
  );
};

export default ConversationList;
