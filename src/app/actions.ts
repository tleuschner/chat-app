import { Socket } from "socket.io-client";
import { ChatMessageWithChatId } from "../mockData";
import {
  MessageActionTypes,
  RECEIVE_MESSAGE,
  SEND_MESSAGE,
  SET_SOCKET,
  AppStateActionTypes,
  SWITCH_CHAT_ROOM,
  SET_USER_ID,
  TOGGLE_CHAT,
} from "./actionTypes";

export const sendMessage = (
  message: ChatMessageWithChatId
): MessageActionTypes => ({
  type: SEND_MESSAGE,
  payload: {
    message,
  },
});

export const receiveMessage = (
  message: ChatMessageWithChatId
): MessageActionTypes => ({
  type: RECEIVE_MESSAGE,
  payload: {
    message,
  },
});

export const switchChatRooms = (chatId: string): MessageActionTypes => ({
  type: SWITCH_CHAT_ROOM,
  payload: { chatId },
});

// APPSTATE //
export const setSocket = (socket: Socket | undefined): AppStateActionTypes => ({
  type: SET_SOCKET,
  payload: {
    socket,
  },
});

export const setUserId = (userId: string): AppStateActionTypes => ({
  type: SET_USER_ID,
  payload: {
    userId,
  },
});

export const toggleChat = (): AppStateActionTypes => ({
  type: TOGGLE_CHAT,
});
