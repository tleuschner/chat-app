import { Socket } from "socket.io-client";
import { applicant } from "../../mockData";
import {
  AppStateActionTypes,
  SET_SOCKET,
  SET_USER_ID,
  TOGGLE_CHAT,
} from "../actionTypes";

interface AppState {
  socket: Socket | undefined;
  userId: string;
  showChat: boolean;
}

const defaultAppState: AppState = {
  socket: undefined,
  userId: applicant,
  showChat: false,
};

const appStateReducer = (
  state = defaultAppState,
  action: AppStateActionTypes
): AppState => {
  switch (action.type) {
    case SET_SOCKET:
      return { ...state, socket: action.payload.socket };
    case SET_USER_ID: {
      return { ...state, userId: action.payload.userId };
    }
    case TOGGLE_CHAT: {
      return { ...state, showChat: !state.showChat };
    }

    default:
      return state;
  }
};

export default appStateReducer;
