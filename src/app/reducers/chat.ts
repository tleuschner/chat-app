import {
  ChatMessage,
  conversationListData,
  ConversationListData,
  facebook,
  kebapp,
  kebappChatMessages,
  matched,
  matchedChatMessages,
} from "../../mockData";
import {
  MessageActionTypes,
  RECEIVE_MESSAGE,
  SEND_MESSAGE,
  SWITCH_CHAT_ROOM,
} from "../actionTypes";

interface ChatState {
  currentChatId: string | null;
  currentChatPartner: ConversationListData | null;
  conversationList: ConversationListData[];
  chatMessages: {
    [chatId: string]: ChatMessage[];
  };
}

const initialState: ChatState = {
  currentChatId: null,
  currentChatPartner: null,
  conversationList: conversationListData,
  chatMessages: {
    [kebapp]: kebappChatMessages,
    [matched]: matchedChatMessages,
    [facebook]: [],
  },
};

const chatStateReducer = (
  state = initialState,
  action: MessageActionTypes
): ChatState => {
  console.log(action);
  switch (action.type) {
    case RECEIVE_MESSAGE: {
      const { message } = action.payload;
      const { chatId } = message;
      return {
        ...state,
        chatMessages: {
          ...state.chatMessages,
          [chatId]: [...state.chatMessages[chatId], message],
        },
        conversationList: state.conversationList.map((partner) => {
          if (partner.chatId === chatId) {
            return {
              ...partner,
              lastMessage: {
                message: message.message,
                //TODO lift this up, not a pure funciton anymore
                timestamp: Date.now(),
              },
            } as ConversationListData;
          }
          return partner;
        }),
      };
    }
    case SEND_MESSAGE: {
      const { message } = action.payload;
      const { chatId } = message;
      return {
        ...state,
        chatMessages: {
          ...state.chatMessages,
          [chatId]: [...state.chatMessages[chatId], message],
        },
        conversationList: state.conversationList.map((partner) => {
          if (partner.chatId === chatId) {
            return {
              ...partner,
              lastMessage: {
                message: message.message,
                //TODO lift this up, not a pure funciton anymore
                timestamp: Date.now(),
              },
            } as ConversationListData;
          }
          return partner;
        }),
      };
    }
    case SWITCH_CHAT_ROOM: {
      const { chatId } = action.payload;
      return {
        ...state,
        currentChatId: chatId,
        currentChatPartner:
          conversationListData.find((p) => p.chatId === chatId) || null,
      };
    }
    default:
      return state;
  }
};

export default chatStateReducer;
