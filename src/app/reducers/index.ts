import { combineReducers } from "@reduxjs/toolkit";
import chat from "./chat";
import app from "./app";

export default combineReducers({ chat, app });
