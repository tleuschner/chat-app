import { createStore } from "@reduxjs/toolkit";
import rootReducer from "./reducers";

export default createStore(rootReducer);

export type RootState = ReturnType<typeof rootReducer>;
