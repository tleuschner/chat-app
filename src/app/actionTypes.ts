import { Socket } from "socket.io-client";
import { ChatMessageWithChatId } from "../mockData";

export const SEND_MESSAGE = "SEND_MESSAGE";
export const RECEIVE_MESSAGE = "RECEIVE_MESSAGE";
export const SWITCH_CHAT_ROOM = "SWITCH_CHAT_ROOM";

interface SendMessageAction {
  type: typeof SEND_MESSAGE;
  payload: { message: ChatMessageWithChatId };
}

interface ReceiveMessageAction {
  type: typeof RECEIVE_MESSAGE;
  payload: { message: ChatMessageWithChatId };
}

interface SwitchChatRoomAction {
  type: typeof SWITCH_CHAT_ROOM;
  payload: { chatId: string };
}

export type MessageActionTypes =
  | SendMessageAction
  | ReceiveMessageAction
  | SwitchChatRoomAction;

// APPSTATE //

export const SET_SOCKET = "SET_SOCKET";
export const SET_USER_ID = "SET_USER_ID";
export const TOGGLE_CHAT = "TOGGLE_CHAT";

interface SetSocketAction {
  type: typeof SET_SOCKET;
  payload: {
    socket: Socket | undefined;
  };
}

interface SetUserIdAction {
  type: typeof SET_USER_ID;
  payload: {
    userId: string;
  };
}

interface ToggleChatAction {
  type: typeof TOGGLE_CHAT;
}

export type AppStateActionTypes =
  | SetSocketAction
  | SetUserIdAction
  | ToggleChatAction;
