import { useEffect, useState } from "react";
import { io, Socket } from "socket.io-client";
const ENDPOINT = "https://immense-badlands-08282.herokuapp.com";

const useSocket = (userId: string, chatIds?: string[]) => {
  const [socket, setSocket] = useState<Socket | undefined>(undefined);

  useEffect(() => {
    const connectToSocket = () => {
      const socketConnection = io(ENDPOINT);
      socketConnection.emit("join", { chatIds, userId });
      setSocket(socketConnection);
    };

    !socket && chatIds && connectToSocket();
  }, [socket, chatIds, userId]);

  return socket;
};

export default useSocket;
